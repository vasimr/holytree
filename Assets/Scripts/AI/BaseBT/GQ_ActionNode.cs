﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate GQ_RunStatus GQ_Action(float dt);

public class GQ_ActionNode : GQ_BTNodeBase
{
    public GQ_Action action = null;


    public GQ_ActionNode() : base()
    {
        name += "ActionNode";
    }

    public GQ_ActionNode(string name, int order = 0) : base(name, order)
    {

    }

    public GQ_ActionNode(string name, GQ_Action action, int order = 0) : base(name, order)
    {
        this.action = action;
    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (!Precondition())
            return GQ_RunStatus.Fail;
        status = action(dt);
        return status;
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        GQ_RunStatus temp = action(dt);
        if (temp == GQ_RunStatus.Running)
            bt_cusTick.ActivedAction.Add(action);
        return temp;
    }
}
