﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//base
public abstract class GQ_CompositeNode : GQ_NONActionNode
{
    protected List<GQ_BTNodeBase> children = new List<GQ_BTNodeBase>();
    public List<GQ_BTNodeBase> Children { get => children; }

    public GQ_CompositeNode()
    {
        name = "NoNameCompositeNode";
    }

    public GQ_CompositeNode(string name, int order = 0) : base(name, order)
    {
       
    }

    public virtual void AddChild(GQ_BTNodeBase bt_Node)
    {
        children.Add(bt_Node);
        children.Sort(SortChildren);
    }

    public virtual void RemoveChild(GQ_BTNodeBase bt_Node)
    {
        if (children.Contains(bt_Node))
            children.Remove(bt_Node);
    }

    protected virtual int SortChildren(GQ_BTNodeBase n1, GQ_BTNodeBase n2)
    {
        if (n1.order > n2.order)
            return 1;
        else if (n1.order < n2.order)
            return -1;
        else
            return 0;
    }
}

//sequence
public class GQ_SequenceNode : GQ_CompositeNode
{
    protected int runningIndex = 0;
    public int RunningIndex { get => runningIndex; }

    public GQ_SequenceNode()
    {
        name = "NoNameSequence";
        order = 0;
    }

    public GQ_SequenceNode(string name, int order = 0) : base(name, order)
    {
        
    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Success);

        GQ_RunStatus tempStatus = children[RunningIndex].Tick(dt);
        if (tempStatus == GQ_RunStatus.Fail)
        {
            runningIndex = 0;
            return SetAndReturn(GQ_RunStatus.Fail);
        }
        else if (tempStatus == GQ_RunStatus.Success)
        {
            ++runningIndex;
            if (IsLastChild())
                return SetAndReturn(GQ_RunStatus.Success);
        }

        return SetAndReturn(GQ_RunStatus.Running);
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Success);

        GQ_RunStatus tempStatus = children[RunningIndex].TickCusTime(bt_cusTick, dt);
        if (tempStatus == GQ_RunStatus.Fail)
        {
            runningIndex = 0;
            return SetAndReturn(GQ_RunStatus.Fail);
        }
        else if (tempStatus == GQ_RunStatus.Success)
        {
            ++runningIndex;
            if (IsLastChild())
                return SetAndReturn(GQ_RunStatus.Success);
        }

        return SetAndReturn(GQ_RunStatus.Running);
    }

    protected bool IsLastChild()
    {
        if (runningIndex >= children.Count)
        {
            runningIndex = 0;
            return true;
        }
        return false;
    }
}

//selector
public class GQ_SelectorNode : GQ_CompositeNode
{
    protected int runningIndex = 0;
    public int RunningIndex { get => runningIndex; }

    public GQ_SelectorNode()
    {
        name = "NoNameSelector";
        order = 0;
    }

    public GQ_SelectorNode(string name, int order = 0) : base(name, order)
    {

    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        GQ_RunStatus tempStatus = children[RunningIndex].Tick(dt);

        if (tempStatus == GQ_RunStatus.Running)
            return SetAndReturn(GQ_RunStatus.Running);

        if (tempStatus == GQ_RunStatus.Success)
        {
            runningIndex = 0;
            return SetAndReturn(GQ_RunStatus.Success);
        }
        ++runningIndex;
        if (IsLastChild())
            return SetAndReturn(GQ_RunStatus.Fail);
        return SetAndReturn(GQ_RunStatus.Running);
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        GQ_RunStatus tempStatus = children[RunningIndex].TickCusTime(bt_cusTick, dt);

        if (tempStatus == GQ_RunStatus.Running)
            return SetAndReturn(GQ_RunStatus.Running);

        if (tempStatus == GQ_RunStatus.Success)
        {
            runningIndex = 0;
            return SetAndReturn(GQ_RunStatus.Success);
        }
        ++runningIndex;
        if (IsLastChild())
            return SetAndReturn(GQ_RunStatus.Fail);
        return SetAndReturn(GQ_RunStatus.Running);
    }

    protected bool IsLastChild()
    {
        if (runningIndex >= children.Count)
        {
            runningIndex = 0;
            return true;
        }
        return false;
    }
}

//同时执行所有子node,有一个失败则返回失败
public class GQ_ParallelNode : GQ_CompositeNode
{
    protected int successNum = 0;
    public int SuccessNum { get => successNum; }

    public GQ_ParallelNode()
    {
        name = "NoNameParallel";
        order = 0;
    }

    public GQ_ParallelNode(string name, int order = 0) : base(name, order)
    {

    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        bool fail = false;

        foreach(GQ_BTNodeBase BTnode in children)
        {
            GQ_RunStatus tempStatus = BTnode.Tick(dt);

            if (tempStatus == GQ_RunStatus.Success)
            {
                ++successNum;
                continue;
            }
            if (tempStatus == GQ_RunStatus.Fail)
            {
                successNum = 0;
                fail = true;
            }
        }
        if (fail)
            return SetAndReturn(GQ_RunStatus.Fail);

        if (successNum == children.Count)
        {
            successNum = 0;
            return SetAndReturn(GQ_RunStatus.Success);
        }
        return SetAndReturn(GQ_RunStatus.Running);
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        //bool fail = false;

        foreach (GQ_BTNodeBase BTnode in children)
        {
            GQ_RunStatus tempStatus = BTnode.TickCusTime(bt_cusTick, dt);

            if (tempStatus == GQ_RunStatus.Success)
            {
                ++successNum;
                continue;
            }
            if (tempStatus == GQ_RunStatus.Fail)
            {
                successNum = 0;
                //fail = true;
                return SetAndReturn(GQ_RunStatus.Fail);
            }
        }
        //if (fail)
        //    return SetAndReturn(GQ_RunStatus.Fail);

        if (successNum == children.Count)
        {
            successNum = 0;
            return SetAndReturn(GQ_RunStatus.Success);
        }
        return SetAndReturn(GQ_RunStatus.Running);
    }
}

//随机选取一个子node执行，并返回其状态
public class GQ_RandomSelector : GQ_CompositeNode
{
    protected int runningIndex = 0;
    public int RunningIndex { get => runningIndex; }

    public GQ_RandomSelector()
    {
        name = "NoNameRandomSelector";
        order = 0;
    }

    public GQ_RandomSelector(string name, int order = 0) : base(name, order)
    {

    }

    public override void AddChild(GQ_BTNodeBase bt_Node)
    {
        children.Add(bt_Node);
    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        if (status != GQ_RunStatus.Running)
        {
            //randomly select
            runningIndex = Random.Range(0, children.Count);
        }

        GQ_RunStatus tempStatus = children[runningIndex].Tick(dt);

        if (tempStatus != GQ_RunStatus.Running)
            return SetAndReturn(tempStatus);

        return SetAndReturn(GQ_RunStatus.Running);
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        if (status != GQ_RunStatus.Running)
        {
            //randomly select
            runningIndex = Random.Range(0, children.Count);
        }

        GQ_RunStatus tempStatus = children[runningIndex].TickCusTime(bt_cusTick, dt);

        if (tempStatus != GQ_RunStatus.Running)
            return SetAndReturn(tempStatus);

        return SetAndReturn(GQ_RunStatus.Running);
    }
}


//同时执行所有的子node，有一个成功则返回成功
public class GQ_ParallelFlex : GQ_CompositeNode
{
    protected int failNum = 0;
    public int FailNum { get => failNum; }

    public GQ_ParallelFlex()
    {
        name = "NoNameParallelFlex";
        order = 0;
    }

    public GQ_ParallelFlex(string name, int order = 0) : base(name, order)
    {

    }

    public override GQ_RunStatus Tick(float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        bool success = false;

        foreach (GQ_BTNodeBase BTnode in children)
        {
            GQ_RunStatus tempStatus = BTnode.Tick(dt);

            if (tempStatus == GQ_RunStatus.Success)
            {
                success = true;
                continue;
            }
            if (tempStatus == GQ_RunStatus.Fail)
            {
                ++failNum;
            }
        }
        if (success)
            return SetAndReturn(GQ_RunStatus.Success);

        if (failNum == children.Count)
        {
            failNum = 0;
            return SetAndReturn(GQ_RunStatus.Fail);
        }
        return SetAndReturn(GQ_RunStatus.Running);
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick, float dt)
    {
        if (!Precondition())
            return SetAndReturn(GQ_RunStatus.Fail);

        if (children.Count == 0)
            return SetAndReturn(GQ_RunStatus.Fail);

        bool success = false;

        foreach (GQ_BTNodeBase BTnode in children)
        {
            GQ_RunStatus tempStatus = BTnode.TickCusTime(bt_cusTick, dt);

            if (tempStatus == GQ_RunStatus.Success)
            {
                success = true;
                continue;
            }
            if (tempStatus == GQ_RunStatus.Fail)
            {
                ++failNum;
            }
        }
        if (success)
            return SetAndReturn(GQ_RunStatus.Success);

        if (failNum == children.Count)
        {
            failNum = 0;
            return SetAndReturn(GQ_RunStatus.Fail);
        }
        return SetAndReturn(GQ_RunStatus.Running);
    }

}


