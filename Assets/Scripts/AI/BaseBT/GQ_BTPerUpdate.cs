﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GQ_BTPerUpdate : MonoBehaviour
{
    public GQ_BTNodeBase entry = null;
    protected bool isRunning = false;

    protected void Awake()
    {
        Init();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Tick(Time.deltaTime);
    }

    protected virtual void Init()
    {

    }

    public void SetRunningStatus(bool status)
    {
        isRunning = status;
        if (status)
            OnStartRunning();
        if (!status)
            OnEndRunning();
    }

    protected virtual void OnStartRunning()
    {

    }

    protected virtual void OnEndRunning()
    {

    }

    protected virtual void Tick(float dt)
    {
        if (entry == null || !isRunning)
            return;
        entry.Tick(dt);
    }
}
