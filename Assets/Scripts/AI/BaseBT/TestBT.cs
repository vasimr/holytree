﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPrecondition : GQ_BTPrecondition
{
    protected TestBT timer;

    public TestPrecondition(TestBT timer)
    {
        this.timer = timer;
    }

    public override bool Precondition()
    {
        if (timer.T1 < 3.0f)
            return true;
        return false;
    }
}

public class TestBT : GQ_BTCusTick
{
    [SerializeField]
    protected float t1 = 0.0f;
    public float T1 { get => t1; }

    [SerializeField]
    protected float t2 = 0.0f;
    public float T2 { get => T2; }

    [SerializeField]
    protected bool someStatus = false;

    protected override void Init()
    {
        t1 = 0.0f;
        t2 = 0;
        GQ_SequenceNode root = new GQ_SequenceNode("testRoot");

        GQ_ParallelNode t = new GQ_ParallelNode("Timer++", 0);
        root.AddChild(t);

        GQ_ActionNode act1 = new GQ_ActionNode("act_1", 0);
        act1.action = Action_1;
        GQ_ActionNode act2 = new GQ_ActionNode("act_2", 1);
        act2.action = Action_2;

        //root.AddChild(act1);
        //root.AddChild(act2);

        t.AddChild(act1);
        t.AddChild(act2);

        GQ_ActionNode act3 = new GQ_ActionNode("act_3", 1);
        act3.action = Action_3;

        //TestPrecondition pre = new TestPrecondition(this);
        //act1.precondition = pre;
        root.AddChild(act3);

        entry = root;

        SetRunningStatus(true);
    }

    protected override void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            someStatus = !someStatus;
        }

        base.Update();
        //Debug.Log(timer);
        
    }

    protected GQ_RunStatus Action_1(float dt)
    {
        t1 += Time.deltaTime;
        if (t1 < 3.0f)
        {
            //Debug.Log("act_1_running");
            return GQ_RunStatus.Running;
        }
            
        //if (t1 > 3.00001f)
        //{
        //    Debug.Log("act_1_fail");
        //    ForcedTick();
        //    return GQ_RunStatus.Fail;
        //}

        //ForcedTick();
        //Debug.Log("act_1_success");
        return GQ_RunStatus.Success;
    }


    protected GQ_RunStatus Action_2(float dt)
    {
        t2 += Time.deltaTime;
        if (t2 < 4.0f)
        {
            //Debug.Log("act_2_running");
            return GQ_RunStatus.Running;
        }
        //Debug.Log("act_2_success");
        //ForcedTick();
        return GQ_RunStatus.Success;
    }

    protected GQ_RunStatus Action_3(float dt)
    {
        //Debug.Log("Act_3_Monitoring");
        if(someStatus)
        {
            t1 = 0;
            t2 = 0;
            //Debug.Log("act_3_success");
            return GQ_RunStatus.Success;
        }
        return GQ_RunStatus.Running;


    }

}
