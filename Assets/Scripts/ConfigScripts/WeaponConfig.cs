using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "WeaponConfig/NewWeaponConfig")]
public class WeaponConfig : BaseItem
{
    [Range(0, float.MaxValue), LabelText("������")]
    public float Attack;
    [Range(0, float.MaxValue), LabelText("������")]
    public float KnockBackPower;
    [Range(0, float.MaxValue), LabelText("�����ٶ�")]
    public float AttackSpeed;

    public override void DestoryOBJ(uint count){}
    public override void UseObj()
    {
        
    }
}
