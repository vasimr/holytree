using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Bag
{
    public static Bag Instance
    {
        get
        {
            if (instance == null)
                instance = new Bag();
            return instance;
        }
    }
    private static Bag instance;

    public List<int>      countList = new List<int>();
    public List<BaseItem> sampleList = new List<BaseItem>();

    public void StoreItem(BaseItem item)
    {
        if (item.ifCanStack == true)
        {
            bool ifNew = true;
            for (int i = 0; i < sampleList.Count; i++)
            {
                if (sampleList[i].ID == item.ID)
                {
                    countList[i] ++;
                    ifNew = false;
                    return;
                }
            }
            if (ifNew)
            {
                BaseItem newItem = CopyItem(item);
                sampleList.Add(newItem);
                countList.Add(1);
            }
        }
        else
        {
            BaseItem newItem = CopyItem(item);
            sampleList.Add(newItem);
            countList.Add(1);
        }
    }

    public void DestroyItem(BaseItem item)
    {
        for (int i = 0; i < sampleList.Count; i++)
        {
            if (sampleList[i].ID == item.ID)
            {
                countList[i]--;
                if(countList[i] == 0)
                {
                    sampleList.RemoveAt(i);
                    countList.RemoveAt(i);
                    return;
                }
            }
               
        }
    }

    public bool CheckItemExist(string itemName)
    {
        for(int i = 0;i < sampleList.Count; i++ )
        {
            if (sampleList[i].OBJname == itemName)
                return true;
        }
        return false;
    }

    private BaseItem CopyItem(BaseItem origin)
    {
        BaseItem result = new BaseItem();
        result.ID = origin.ID;
        result.sprite = origin.sprite;
        result.OBJname = origin.OBJname;
        result.itemType = origin.itemType;
        result.ifCanUse = origin.ifCanUse;
        result.ifCanStack = origin.ifCanStack;
        result.OBJdescribe = origin.OBJdescribe;
        result.maxStackCount = origin.maxStackCount;
        return result;
    }
}
