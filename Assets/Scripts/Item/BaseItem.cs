using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public enum ItemType
{
    All = 0,
    Weapon = 1,
    Consume = 2,
    Other = 3,
}

public class BaseItem : ScriptableObject
{
    [LabelText("物品ID")]
    public int ID = 0;
    [LabelText("物品图像")]
    public Sprite sprite;
    [LabelText("物品类型")]
    public ItemType itemType = ItemType.Other;
    [LabelText("物品名称")]
    public string OBJname = "";
    [LabelText("物品描述")]
    public string OBJdescribe = "";

    [LabelText("物体是否可交互")]
    public bool ifCanUse = false;

    [LabelText("是否可堆叠")]
    public bool ifCanStack = false;
    [Range(0, 99), LabelText("最大堆叠数量")]
    public uint maxStackCount = 0;

    virtual public void UseObj() { }      // 使用物品
    virtual public void DestoryOBJ(uint count) { }  // 销毁物品

}