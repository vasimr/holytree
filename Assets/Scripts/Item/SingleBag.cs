using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SingleBag : MonoBehaviour, IPointerClickHandler
{
    public Image image;
    public Text countNum;
    private BaseItem showItem = null;

    public Action<BaseItem,Vector3> OpenItemDetail; 

    public void BindOBJ(BaseItem item,int count )
    {
        image.sprite = item.sprite;
        countNum.text = count.ToString();
        showItem = item;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OpenItemDetail?.Invoke(showItem,transform.position);
    }
}
