using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SingleItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Image image;
    public Text itemName;
    public Text describe;

    public ItemType itemType = ItemType.All;

    private BaseItem showItem = null;

    public Action<ItemType> ShowItem;

    public void BindItem(BaseItem item)
    {
        if (item.itemType == itemType)
        {
            showItem = item;
            image.sprite = item.sprite;
            itemName.text = item.OBJname;
            describe.text = item.OBJdescribe;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ShowItem?.Invoke(itemType);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        describe.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        describe.gameObject.SetActive(false);
    }
}
