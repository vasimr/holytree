using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BagWindow : MonoBehaviour
{
    Bag bag = Bag.Instance;
    public GameObject singlebag;
    public GameObject content;
    public GameObject bagBar;
    public Button bg;

    public GameObject detailOBJ;
    public Text itemName;
    public Text itemDescribe;
    public Button useBTN;

    List<GameObject> singleBarList = new List<GameObject>();

    Dictionary<ItemType, SingleItem> itemDic = new Dictionary<ItemType, SingleItem>();

    private void Start()
    {
        SingleItem[] singleList = GetComponentsInChildren<SingleItem>();
        foreach(SingleItem item in singleList)
        {
            item.ShowItem = ShowItemByType;
            itemDic[item.itemType] = item;
        }
        bg.onClick.AddListener(Closebar);
        bg.onClick.AddListener(CloseDetailWindow);
    }

    private void Closebar()
    {
        for(int i = 0;i < singleBarList.Count ; i++)
        {
            Destroy(singleBarList[i]);
        }
        singleBarList.Clear();
        bagBar.gameObject.SetActive(false);
        bg.gameObject.SetActive(false);
    }

    public void ShowItemByType(ItemType itemType)
    {
        bg.gameObject.SetActive(true);
        bagBar.gameObject.SetActive(true);

        for (int i = 0; i < singleBarList.Count; i++)
        {
            Destroy(singleBarList[i]);
        }
        singleBarList.Clear();

        for (int i = 0 ; i < bag.sampleList.Count; i++)
        {
            if(bag.sampleList[i].itemType == itemType)
            {
               GenerateSingelBar(bag.sampleList[i], bag.countList[i]);
            }
        }
    }

    public void GenerateSingelBar(BaseItem item,int count)
    {
        if(singlebag != null)
        {
            GameObject newBar = Instantiate(singlebag);
            newBar.transform.SetParent(content.transform);
            SingleBag bar = newBar.GetComponent<SingleBag>();
            bar.OpenItemDetail = OpenDetailWindow;
            bar.BindOBJ(item, count);
            singleBarList.Add(newBar);
        }
    }

    private void CloseDetailWindow()
    {
        bg.gameObject.SetActive(false);
        detailOBJ.gameObject.SetActive(false);
    }

    BaseItem curShowItem = null;
    public void OpenDetailWindow(BaseItem item, Vector3 newPos)
    {
        bg.gameObject.SetActive(true);
        detailOBJ.gameObject.SetActive(true);
        itemName.text = item.OBJname;
        itemDescribe.text = item.OBJdescribe;
        if (item.ifCanUse)
        {
            useBTN.gameObject.SetActive(true);
            curShowItem = item;
            useBTN.onClick.AddListener(EquipeItem);
        }          
        else
            useBTN.gameObject.SetActive(false);

        detailOBJ.transform.position = newPos;
    }

    public void EquipeItem()
    {
       if (itemDic.ContainsKey(curShowItem.itemType))
           itemDic[curShowItem.itemType].BindItem(curShowItem);
    }

}
