using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MapManager : BaseManager
{
    public const string BIG_WORLD_MAP_CONFIG_NAME = "BigWorldMap";

    public Maze BigWorldMap { private set; get; }
    public bool IsInited { get; }

    public override void Init()
    {
        BigWorldMap = Maze.FromMapConfig(Path.Combine(MazeSettings.MAZE_CONFIG_DIR, BIG_WORLD_MAP_CONFIG_NAME));
        base.Init();
    }
}
