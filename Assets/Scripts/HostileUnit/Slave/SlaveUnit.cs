using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlaveUni : HostileUnitBase
{
    public SlaveConfig Config;

    protected bool attacking = false;
    protected Coroutine attackCoroutine = null;
    protected float attackCDTimer = 0;

    protected void Update()
    {
        CDUpdate();
    }

    public override bool ReceiveDamage(int damage, int impact /*, fromTarget*/)
    {
        return true;
    }

    public override bool ReceiveHealing(int healing /*, fromTarget*/)
    {
        return true;
    }

    public override bool Move(Vector2 dir)
    {
        if (attacking)
            return false;
        physicsComp.velocity = dir.normalized * Config.speed;
        IsFacingRight = dir.x > 0;
        return true;
    }

    public override bool Attack(Vector2 dir)
    {
        if (attacking)
            return false;
        if (attackCDTimer > 0)
            return false;
        var ienum = AttackCoroutine(dir.normalized);
        attackCoroutine = StartCoroutine(ienum);
        return true;
    }

    protected IEnumerator AttackCoroutine(Vector2 dir)
    {
        //准备攻击开始后不能停止
        attacking = true;
        //准备攻击
        IsFacingRight = dir.x > 0;
        yield return new WaitForSeconds(Config.attackPrepare);
        //攻击
        //TODO
        attacking = false;
        attackCDTimer = Config.attackCD;
    }

    protected void CDUpdate()
    {
        if (attackCDTimer > 0)
            attackCDTimer -= Time.deltaTime;
    }

    private void OnDestroy()
    {
        if (attackCoroutine != null)
            StopCoroutine(attackCoroutine);
    }
}
