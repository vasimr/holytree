using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D))]
public class HostileUnitBase : MonoBehaviour
{
    //属性
    //SpriteRenderer
    public SpriteRenderer sprite = null;
    //physics component
    public Rigidbody2D physicsComp = null;

    protected bool isFacingRight = true;

    #region GETSET
    public bool IsFacingRight { 
        get { return isFacingRight; } 
        set { isFacingRight = value; OnDirectionChange(); } 
    }
    #endregion

    protected void Start()
    {
        InitComponent();
    }

    protected virtual void InitComponent()
    {
        physicsComp = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    protected void OnDirectionChange()
    {
        sprite.flipX = !isFacingRight;
    }

    public virtual bool ReceiveDamage(int damage, int impact /*, fromTarget*/)
    {
        return false;
    }

    public virtual bool ReceiveHealing(int healing /*, fromTarget*/)
    {
        return false;
    }

    //返回该操作是否成功
    public virtual bool Attack(Vector2 dir)
    {
        return false;
    }

    public virtual bool Move(Vector2 dir)
    {
        return false;
    }
}
