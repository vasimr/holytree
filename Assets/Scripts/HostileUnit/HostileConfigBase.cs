using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileConfigBase : ScriptableObject
{
    [LabelText("�����ʼѪ��")]
    public float maxHP = 100.0f;

    [LabelText("������")]
    public float ATK = 20.0f;

    [Range(0.01f, 5.0f), LabelText("�ƶ��ٶ�")]
    public float speed = 1.0f;

    [Range(0f, 1.0f), LabelText("����׼��ʱ��")]
    public float attackPrepare = 0.5f;

    [Range(0f, 10.0f), LabelText("������ȴʱ��")]
    public float attackCD = 3.0f;

    [Range(0f, 20.0f), LabelText("׷������")]
    public float chaseDistance = 7.0f;          //ָ�뿪����λ�õľ���,�����˾��������׷��

    [Range(0f, 10.0f), LabelText("�۲����")]
    public float obDistance = 4.0f;

    [Range(0f, 2.5f), LabelText("��������")]
    public float attackRange = 0.75f;

    [Range(0f, 5.0f), LabelText("Ѳ�߾���")]
    public float patrolDistance = 2.0f;

    [Range(0f, 2.0f), LabelText("Ѳ�߼��MIN")]
    public float patrolCDmin = 1.0f;

    [Range(2.0f, 5.0f), LabelText("Ѳ�߼��MAX")]
    public float patrolCDmax = 3.0f;

    [Range(10.0f, 20.0f), LabelText("AI˯�߾���")]
    public float aiSleepDis = 15.0f;
}
