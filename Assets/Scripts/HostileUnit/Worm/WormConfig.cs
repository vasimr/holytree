using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "HostileConfig/WormConfig")]
public class WormConfig : HostileConfigBase
{
    [Range(0f, 500f), LabelText("�����������")]
    public int rushPower = 150;

    [LabelText("���LinearDrag")]
    public int linearDrag = 9;

    [Range(0f, 1.0f), LabelText("�����ж�����ʱ��")]
    public float attackDuration = 0.5f;
}
