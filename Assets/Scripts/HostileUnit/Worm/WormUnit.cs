using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormUnit : HostileUnitBase
{
    public WormConfig Config;

    protected bool attacking = false;
    protected bool attackAreaActive = false;
    protected Coroutine attackCoroutine = null;
    protected float attackCDTimer = 0;

    protected Animator animator = null;

    protected void Update()
    {
        CDUpdate();
        AnimatorUpdate();
    }

    protected override void InitComponent()
    {
        base.InitComponent();
        physicsComp.drag = Config.linearDrag;
        animator = gameObject.GetComponent<Animator>();
    }

    public override bool ReceiveDamage(int damage, int impact /*, fromTarget*/)
    {
        return true;
    }

    public override bool ReceiveHealing(int healing /*, fromTarget*/)
    {
        return true;
    }

    public override bool Move(Vector2 dir)
    {
        if (attacking)
            return false;
        physicsComp.velocity = dir.normalized * Config.speed;
        IsFacingRight = dir.x > 0;
        return true;
    }

    public override bool Attack(Vector2 dir)
    {
        if (attacking)
            return false;
        if (attackCDTimer > 0)
            return false;
        var ienum = AttackCoroutine(dir.normalized);
        attackCoroutine = StartCoroutine(ienum);
        return true;
    }

    protected IEnumerator AttackCoroutine(Vector2 dir)
    {
        //准备攻击开始后不能停止
        attacking = true;
        //准备攻击
        IsFacingRight = dir.x > 0;
        animator.SetBool("AttackPreparing", true);
        yield return new WaitForSeconds(Config.attackPrepare);
        //攻击
        attackAreaActive = true;
        physicsComp?.AddForce(dir * Config.rushPower);
        animator.SetBool("AttackPreparing", false);
        animator.SetBool("Attacking", true);
        yield return new WaitForSeconds(Config.attackDuration);
        animator.SetBool("Attacking", false);
        attackAreaActive = false;
        attacking = false;
        attackCDTimer = Config.attackCD;
    }

    protected void CDUpdate()
    {
        if(attackCDTimer > 0)
            attackCDTimer -= Time.deltaTime;
    }

    protected void AnimatorUpdate()
    {
        animator.SetFloat("Speed", physicsComp.velocity.magnitude);
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (!attackAreaActive)
            return;
        if (collision.collider.gameObject.tag != "Player")
            return;
        //处理攻击...

        attackAreaActive = false;
    }

    private void OnDestroy()
    {
        if(attackCoroutine != null)
            StopCoroutine(attackCoroutine);
    }
}
