using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WormUnit))]
public class WormAI : GQ_BTPerUpdate
{
    public WormUnit worm = null;
    //global player target, waiting...
    public GameObject target = null;
    public GameObject fakeGlobalTarget = null;

    //Ѳ�����
    protected Vector2 orgPosition = Vector2.zero;

    protected Vector2 lastPatrolTarget = Vector2.zero;
    protected float patrolDistance = 0;
    protected Vector2 patrolDir = Vector2.zero;
    protected float patrolCDTimer = 0;
    protected bool noPatrolTarget = true;

    protected WormConfig Config { get { return worm.Config; } }

    protected override void Init()
    {
        worm = GetComponent<WormUnit>();
        orgPosition = transform.position;
        RandomPatrolCD();
        FormBehaviourTree();
    }

    protected override void Start()
    {
        SetRunningStatus(true);
    }

    protected override void Update()
    {
        //if player is too far away, sleep.
        if (Vector2.Distance(fakeGlobalTarget.transform.position, transform.position) > Config.aiSleepDis)
            return;

        base.Update();
    }

    //form a behaviour tree with code.
    protected void FormBehaviourTree()
    {
        var root = new GQ_SelectorNode("root");
        //node0, randomly patrol
        var node0 = new GQ_ActionNode("patrol", 0);
        node0.action = Patrol;
        root.AddChild(node0);
        //node1, target found, battle
        var node1 = new GQ_SequenceNode("battle", 1);
        root.AddChild(node1);
        //node1-0, check is player in range, otherwise move.
        var node1_0 = new GQ_ActionNode("chase", 0);
        node1_0.action = Chase;
        node1.AddChild(node1_0);
        //node1-1, attack
        var node1_1 = new GQ_ActionNode("attack", 1);
        node1_1.action = Attack;
        node1.AddChild(node1_1);

        entry = root;
    }

    #region Actions

    protected GQ_RunStatus Patrol(float dt)
    {
        if(target != null)
            return GQ_RunStatus.Fail;
        //CD
        if(patrolCDTimer > 0)
        {
            patrolCDTimer -= dt;
            return GQ_RunStatus.Running;
        }
        //Generate target
        if (noPatrolTarget)
        {
            lastPatrolTarget = transform.position;
            RandomPatrolTarget();
            noPatrolTarget = false;
        }
        //�ƶ�
        worm.Move(patrolDir);
        //���Ŀ��
        CheckTargetInSight();
        //check if reach the target position
        float dis = Vector2.Distance(transform.position, lastPatrolTarget);
        if(dis >= patrolDistance)
        {
            noPatrolTarget = true;
            RandomPatrolCD();
            return GQ_RunStatus.Success;
        }
        return GQ_RunStatus.Running;
    }

    protected GQ_RunStatus Chase(float dt)
    {
        if(target == null)
            return GQ_RunStatus.Fail;
        //����Ƿ���빥����Χ
        float distance = Vector2.Distance(transform.position, target.transform.position);
        if(distance < Config.attackRange)
            return GQ_RunStatus.Success;
        //����Ƿ񳬳�׷����Χ
        float distanceWithOrg = Vector2.Distance(transform.position, orgPosition);
        if(distanceWithOrg > Config.chaseDistance) 
        {
            target = null;
            return GQ_RunStatus.Fail;
        }
        var nextPos = PathFind.DirectlyMove(target.transform.position, transform.position);
        worm.Move(nextPos);
        return GQ_RunStatus.Running;
    }

    protected GQ_RunStatus Attack(float dt)
    {
        if(worm.Attack(target.transform.position - transform.position))
            return GQ_RunStatus.Success;
        else
            return GQ_RunStatus.Fail;
    }
    #endregion

    #region Assit
    protected void RandomPatrolCD()
    {
        patrolCDTimer = Random.Range(Config.patrolCDmin, Config.patrolCDmax);
    }

    protected void RandomPatrolTarget()
    {
        var patrolTargetPos = orgPosition + Random.insideUnitCircle * Config.patrolDistance;
        patrolDir = patrolTargetPos - new Vector2(transform.position.x, transform.position.y);
        patrolDistance = patrolDir.magnitude;
        patrolDir.Normalize();
    }

    protected void CheckTargetInSight()
    {
        if (!fakeGlobalTarget)
            return;
        var subVec = fakeGlobalTarget.transform.position - transform.position;
        if (subVec.x > 0 && !worm.IsFacingRight)
            return;
        if (subVec.x < 0 && worm.IsFacingRight)
            return;
        if (subVec.magnitude < Config.obDistance)
            target = fakeGlobalTarget;
    }
    #endregion
}
