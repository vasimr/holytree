namespace System.Collections.Generic
{
    using Random = UnityEngine.Random;

    public class RandomQueue<T>
    {
        private LinkedList<T> queue = new();

        public int Count => queue.Count;

        public bool Empty => Count <= 0;

        public void Enqueue(T newElement)
        {
            var rand = Random.Range(0, 100);
            if (rand <= 50)
            {
                queue.AddFirst(newElement);
            }
            else
            {
                queue.AddLast(newElement);
            }
        }

        public T Dequeue()
        {
            if (queue.Count <= 0)
            {
                return default;
            }

            var rand = Random.Range(0, 100);
            if (rand <= 50)
            {
                var element = queue.First;
                queue.RemoveFirst();
                return element.Value;
            }
            else
            {
                var element = queue.Last;
                queue.RemoveLast();
                return element.Value;
            }
        }

        public bool Contains(T element)
        {
            return queue.Contains(element);
        }
    }
}
