using UnityEngine;

public interface IInitializable
{
    public void Init();
    public bool IsInited { get; }
}

public abstract class BaseManager: MonoBehaviour, IInitializable
{
    public bool IsInited { private set; get; }
    public virtual void Init()
    {
        IsInited = true;
    }
}

public class UnityObjectSingleton<T>
    where T: Component
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Object.FindObjectOfType<T>();
                if (_instance == null)
                {
                    var newObj = new GameObject(typeof(T).Name);
                    _instance = newObj.AddComponent<T>();
                }
                if (_instance is IInitializable { IsInited: false } initializable)
                {
                    initializable.Init();
                }
            }
            return _instance;
        }
    }
}
