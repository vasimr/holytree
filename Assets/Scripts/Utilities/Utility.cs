using System.Collections.Generic;

public static class Utility
{
    public static bool IsNullOrEmpty(this string str)
    {
        return string.IsNullOrEmpty(str);
    }

    public static bool IsNullOrEmpty<T>(this T[] arr)
    {
        return arr == null || arr.Length == 0;
    }

    public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
    {
        return collection == null || collection.Count == 0;
    }

    public static string UnifySeparators(this string str)
    {
        return str.Replace("\\", "/");
    }
}
