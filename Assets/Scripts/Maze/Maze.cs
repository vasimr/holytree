using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class Maze
{
    public Tile[,] Map;
    public int Size;
    // public bool[,] IsPath { private set; get; }

    private readonly bool[,] visited;
    private readonly int[,] direction = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
    public readonly bool[,] solutionVisited;

    public readonly int entranceX = 0;
    public readonly int entranceY = 1;
    public readonly int exitX;
    public readonly int exitY;

    public static readonly Vector2 WORLD_ORIGIN = Vector2.zero;
    public static readonly float UNIT_SIZE = 128;

    public Maze()
    {
        Size = 40;
        exitX = Size - 1;
        exitY = Size - 2;
        Map = new Tile[Size, Size];
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                Map[x, y] = new Tile();
            }
        }
        visited = new bool[Size, Size];
        solutionVisited = new bool[Size, Size];
    }

    public Maze(int size)
    {
        Size = size % 2 == 0 ? size + 1 : size;
        exitX = Size - 1;
        exitY = Size - 2;
        Map = new Tile[Size, Size];
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                Map[x, y] = new Tile();
            }
        }
        visited = new bool[Size, Size];
        // IsPath = new bool[Size, Size];
        solutionVisited = new bool[Size, Size];
        Reset();
    }

    public static Maze FromMapConfig(string pathRelativeToResources)
    {
        var configFile = Resources.Load<TextAsset>(pathRelativeToResources.UnifySeparators());
        if (configFile == null)
        {
            return null;
        }

        var jObj = JObject.Parse(configFile.text);
        return jObj.ToObject<Maze>();
    }

    public bool InArea(int x, int y)
    {
        return x >= 0 && x < Size && y >= 0 && y < Size;
    }
    public void Reset()
    {
        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                if (i % 2 == 1 && j % 2 == 1)
                {
                    Map[i, j].Type = TileType.Road;
                }
                else
                {
                    if (i != 0 && i != Size - 1 && j != 0 && j != Size - 1)
                    {
                        Map[i, j].Type = Random.Range(0, 101) < 80 ? TileType.Obstacle : TileType.Road;
                    }
                    else
                    {
                        Map[i, j].Type = TileType.Obstacle;
                    }
                }
                visited[i, j] = false;
                solutionVisited[i, j] = false;
                // IsPath[i, j] = false;
            }
        }
        //Map[entranceX,entranceY] = TileType.Road;
        //Map[exitX,exitY] = TileType.Road;

        var queue = new RandomQueue<(int, int)>();
        var first = (entranceX + 1, entranceY);
        queue.Enqueue(first);
        while (!queue.Empty)
        {
            var currentPoint = queue.Dequeue();
            for (int i = 0; i < 4; i++)
            {
                int newX = currentPoint.Item1 + direction[i, 0] * 2;
                int newY = currentPoint.Item2 + direction[i, 1] * 2;

                if (InArea(newX, newY)
                        && !visited[newX, newY]
                        && Map[newX, newY].Type == TileType.Road)
                {
                    queue.Enqueue((newX, newY));
                    visited[newX, newY] = true;
                    Map[currentPoint.Item1 + direction[i, 0], currentPoint.Item2 + direction[i, 1]].Type = TileType.Road;
                }
            }
        }
    }

    public (int, int) GetCoordinateFromWorldPos(Vector2 worldPos)
    {
        worldPos += Vector2.one * UNIT_SIZE;
        worldPos -= WORLD_ORIGIN;

        worldPos.x = Mathf.Max(0, worldPos.x);
        worldPos.y = Mathf.Max(0, worldPos.y);

        var result = ((int)(worldPos.x / UNIT_SIZE), (int)(worldPos.y / UNIT_SIZE));

        result.Item1 = Mathf.Min(result.Item1, Size - 1);
        result.Item2 = Mathf.Min(result.Item2, Size - 1);

        return result;
    }

    //public Stack<Position> Solve(int fromX, int fromY, int toX, int toY)
    //{
    //    Stack<Position> result = null;
    //    for (int i = 0; i < Size; i++)
    //    {
    //        for (int j = 0; j < Size; j++)
    //        {
    //            solutionVisited[i, j] = false;
    //            IsPath[i, j] = false;
    //        }
    //    }
    //    bool isSolved = false;
    //    Queue<Position> queue = new Queue<Position>();
    //    Position entrance = new Position(fromX, fromY);
    //    queue.Enqueue(entrance);
    //    visited[entrance.X, entrance.Y] = true;

    //    while (queue.Count != 0)
    //    {
    //        Position currentPosition = queue.Dequeue();

    //        if (currentPosition.X == toX && currentPosition.Y == toY)
    //        {
    //            result = findPath(currentPosition);
    //            isSolved = true;
    //            break;
    //        }

    //        for (int i = 0; i < 4; i++)
    //        {
    //            int newX = currentPosition.X + direction[i, 0];
    //            int newY = currentPosition.Y + direction[i, 1];

    //            if (InArea(newX, newY)
    //                    && !solutionVisited[newX, newY]
    //                    && Map[newX, newY] == TileType.Road)
    //            {
    //                Position nextPos = new Position(newX, newY, currentPosition);
    //                queue.Enqueue(nextPos);
    //                solutionVisited[newX, newY] = true;
    //            }
    //        }
    //    }
    //    if (!isSolved)
    //    {
    //        throw new Exception("Unsolvable Map!");
    //    }
    //    return result;
    //}
    //private Stack<Position> findPath(Position destination)
    //{
    //    var result = new Stack<Position>();
    //    Position currentPosition = destination;
    //    while (currentPosition != null)
    //    {
    //        result.Push(currentPosition);
    //        IsPath[currentPosition.X, currentPosition.Y] = true;
    //        currentPosition = currentPosition.From;
    //    }
    //    result.Pop();
    //    return result;
    //}
}

