using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosTester : MonoBehaviour
{
    private Maze bigWorld;

    private void Awake()
    {
        bigWorld = UnityObjectSingleton<MapManager>.Instance.BigWorldMap;
    }

    void Update()
    {
        Debug.Log(bigWorld.GetCoordinateFromWorldPos(transform.position));
    }
}
