using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;

public class MazeTester : MonoBehaviour
{
    public const string MAZE_ASSET_PATH = "Assets/Maze.json";
    private Maze maze;

    [SerializeField]
    private TextAsset mazeConfig;

    private void Awake()
    {
        //if (mazeConfig == null)
        //{
        //    maze = new Maze(20);

        //    var mazeConfig = JObject.FromObject(maze);
        //    File.WriteAllText(MAZE_ASSET_PATH, mazeConfig.ToString());
        //}
        //else
        //{
        //    var json = JObject.Parse(mazeConfig.text);
        //    maze = json.ToObject<Maze>();
        //}
        maze = UnityObjectSingleton<MapManager>.Instance.BigWorldMap;
    }

    private void OnDrawGizmos()
    {
        if (maze == null)
        {
            return;
        }
        for (int x = 0; x < maze.Size; x++)
        {
            for (int y = 0; y < maze.Size; y++)
            {
                if (maze.Map[x, y].Type == TileType.Road)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawCube(new Vector3(x, y, 0) * 128, Vector3.one * 128);
                }
                else
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawCube(new Vector3(x, y, 0) * 128, Vector3.one * 128);
                }
            }
        }
    }
}
