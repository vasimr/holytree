using System.IO;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

public class MazeEditorWindow : EditorWindow
{
    [MenuItem("EditorTool/MazeEditor")]
    public static void Present()
    {
        var newWindow = GetWindow<MazeEditorWindow>();
        newWindow.Show();
    }

    public static readonly Vector2 orgBoxSize = new (128f, 128f);

    private uint mazeSize = 40;
    private static float scale = 0.2f;
    private static Vector2 boxSize = orgBoxSize * scale;

    private Maze mazeData;

    private uint newMazeSize;

    private Texture bigWorldMapTexture;

    private static readonly Color LIGHT_BLUE_COLOR = new (0, 0, 1, 0.7f);
    private static readonly Color LIGHT_GREEN_COLOR = new (0, 1, 0, 0.7f);
    private static readonly Color LIGHT_RED_COLOR = new (1, 0, 0, 0.7f);

    private void OnEnable()
    {
        bigWorldMapTexture = Resources.Load<Texture>("BigWorldMapImages/BigWorldMapImage");

        ResetMaze();
    }

    private void ResetMaze()
    {
        newMazeSize = mazeSize;
        mazeData = Maze.FromMapConfig(Path.Combine(MazeSettings.MAZE_CONFIG_DIR, MapManager.BIG_WORLD_MAP_CONFIG_NAME)) ?? new Maze();
    }

    private void OnGUI()
    {
        using (new GUILayout.VerticalScope())
        {
            using (new GUILayout.HorizontalScope())
            {
                using (new GUILayout.HorizontalScope(GUI.skin.box))
                {
                    using (var changeCheck = new EditorGUI.ChangeCheckScope())
                    {
                        scale = EditorGUILayout.FloatField("地图缩放", scale);
                        if (changeCheck.changed)
                        {
                            scale = Mathf.Max(0.1f, scale);
                            boxSize = orgBoxSize * scale;
                        }
                    }
                }
                using (new GUILayout.HorizontalScope(GUI.skin.box))
                {
                    newMazeSize = (uint)EditorGUILayout.IntField("地图边长", (int)newMazeSize);
                    if (GUILayout.Button("重置边长", GUILayout.Width(100))
                        && EditorUtility.DisplayDialog("确定操作", "该操作会重置地图数据，确定？", "确定", "取消"))
                    {
                        ResetMaze();
                    }

                    if (GUILayout.Button("生成地图数据", GUILayout.Width(100)))
                    {
                        var jObj = JObject.FromObject(mazeData);
                        File.WriteAllText(Path.Combine("Assets/Resources", MazeSettings.MAZE_CONFIG_DIR, "BigWorldMap.json"), jObj.ToString());
                        AssetDatabase.Refresh();
                        EditorUtility.DisplayDialog("成功", "处理完毕", "确定");
                    }
                }
            }

            using (var scope = new EditorGUILayout.VerticalScope(GUI.skin.box, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
            {
                var bigWorldImage = new Rect(scope.rect.position, 40 * boxSize);
                EditorGUI.DrawPreviewTexture(bigWorldImage, bigWorldMapTexture);
                for (int x = 0; x < mazeSize; x++)
                {
                    for (int y = 0; y < mazeSize; y++)
                    {
                        var rect = new Rect(scope.rect.position + new Vector2(x * boxSize.x, bigWorldImage.height - y * boxSize.y),
                            boxSize);
                        if (rect.Contains(Event.current.mousePosition))
                        {
                            if (mazeData.Map[x, y].Type == TileType.Road)
                            {
                                EditorGUI.DrawRect(rect, LIGHT_BLUE_COLOR);
                                if (GUI.Button(rect, GUIContent.none, GUIStyle.none))
                                {
                                    mazeData.Map[x, y].Type = TileType.Obstacle;
                                }
                            }
                            else
                            {
                                EditorGUI.DrawRect(rect, LIGHT_RED_COLOR);
                                if (GUI.Button(rect, GUIContent.none, GUIStyle.none))
                                {
                                    mazeData.Map[x, y].Type = TileType.Road;
                                }
                            }
                        }
                        else if (mazeData.Map[x, y].Type == TileType.Obstacle)
                        {
                            EditorGUI.DrawRect(rect, LIGHT_GREEN_COLOR);
                        }
                    }
                }
            }
        }
    }

    private void Update()
    {
        Repaint();
    }
}
